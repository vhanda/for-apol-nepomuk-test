#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>
#include <QtCore/QTime>

#include <Soprano/Statement>
#include <Soprano/Node>
#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/StatementIterator>
#include <Soprano/NodeIterator>
#include <Soprano/PluginManager>

#include <KDebug>

// Vocabularies
#include <Soprano/Vocabulary/RDF>
#include <Soprano/Vocabulary/RDFS>
#include <Soprano/Vocabulary/NRL>
#include <Soprano/Vocabulary/NAO>

#include <Nepomuk2/Vocabulary/NIE>
#include <Nepomuk2/Vocabulary/NFO>
#include <Nepomuk2/Vocabulary/NMM>
#include <Nepomuk2/Vocabulary/NCO>
#include <Nepomuk2/Vocabulary/PIMO>
#include <Nepomuk2/Vocabulary/NCAL>

#include <KUrl>
#include <KJob>

#include <Nepomuk2/Resource>
#include <Nepomuk2/ResourceManager>
#include <Nepomuk2/Tag>
#include <Nepomuk2/Variant>
#include <Nepomuk2/Query/QueryServiceClient>
#include <Nepomuk2/Query/ResourceTypeTerm>
#include <Nepomuk2/Query/LiteralTerm>

#include <nepomuk2/datamanagement.h>
#include <nepomuk2/simpleresource.h>
#include <nepomuk2/simpleresourcegraph.h>
#include <nepomuk2/storeresourcesjob.h>
#include <nepomuk2/resultiterator.h>

#include <iostream>
#include <QtCore/QThread>

using namespace Soprano::Vocabulary;
using namespace Nepomuk2::Vocabulary;

class TestObject : public QObject {
    Q_OBJECT
public slots:
    void main();
    void quit();
    void newEntries(const QList<Nepomuk2::Query::Result>& list );
public:
    TestObject() {
        QTimer::singleShot( 0, this, SLOT(main()) );
        //QTimer::singleShot( 5000, this, SLOT(quit()) );
    }

    virtual ~TestObject() {
        kDebug() << "Removing";
        res.remove();
    }
    Nepomuk2::Resource res;
};

int main( int argc, char ** argv ) {
    KComponentData component( QByteArray("nepomuk-test") );
    QCoreApplication app( argc, argv );

    TestObject a;
    app.exec();
}

void TestObject::quit()
{
    QCoreApplication::instance()->quit();
}

void TestObject::main()
{
    Nepomuk2::Query::ResourceTypeTerm term( NCO::PersonContact() );
    Nepomuk2::Query::Query q( term );

    Nepomuk2::Query::QueryServiceClient* client = new Nepomuk2::Query::QueryServiceClient( this );
    client->query( q );
    kDebug() << q.toSparqlQuery();
    connect( client, SIGNAL(newEntries(QList<Nepomuk2::Query::Result>)), this,  SLOT(newEntries(QList<Nepomuk2::Query::Result>)) );

    return;

    /*
    Nepomuk::Resource res("file:///home/vishesh/TODO");
    kDebug() << "Creating - TimeElapsed: " << timer.elapsed();
    timer.restart();

    int rating = res.rating();
    kDebug() << "Fetchign the rating - TimeElapsed: " << timer.elapsed();
    timer.restart();

    rating = (rating + 1) % 10;
    res.setRating( rating );
    kDebug() << "Setting the rating - TimeElapsed: " << timer.elapsed();
    timer.restart();

    kDebug() << "Getting the rating";
    rating = res.rating();
    kDebug() << "Fetching the rating - TimeElapsed: " << timer.elapsed();
    timer.restart();

    kDebug() << "Getting the rating";
    rating = res.rating();
    kDebug() << "Fetching the rating - TimeElapsed: " << timer.elapsed();
    timer.restart();
*/
}

void TestObject::newEntries(const QList< Nepomuk2::Query::Result >& list)
{
    foreach( const Nepomuk2::Query::Result &r, list ) {
        Nepomuk2::Resource res = r.resource();
        kDebug() << res.uri();
        QHash<QUrl, Nepomuk2::Variant> prop = res.properties();
        QHashIterator<QUrl, Nepomuk2::Variant> it( prop );
        while( it.hasNext() ) {
            it.next();
            kDebug() << "\t" << it.key() << " " << it.value();
        }
        kDebug() << "--------------------------";
    }
}

#include "main.moc"
